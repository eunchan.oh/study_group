#include <stdio.h>
#include "dummy_header.h"

int CompareForSort(LData d1, LData d2);

int main(void)
{
	List list;
	int data;

	// 초기화
	LInit(&list);

	// 정렬 기준 정의
	SetSortRule(&list, CompareForSort);

	// 삽입
	LInsert(&list, 11);  LInsert(&list, 11);
	LInsert(&list, 22);  LInsert(&list, 22);
	LInsert(&list, 33);

	// 조회
	printf("현재 데이터의 수: %d \n", LCount(&list));
	if (LFirst(&list, &data))
	{
		printf("%d ", data);

		while (LNext(&list, &data))
		{
			printf("%d ", data);
		}
	}
	printf("\n\n");

	// 삭제
	if (LFirst(&list, &data))
	{
		if (data == 22)
		{
			LRemove(&list);
		}

		while (LNext(&list, &data))
		{
			if (data == 22)
			{
				LRemove(&list);
			}
		}
	}

	// 삭제 후 조회
	printf("현재 데이터의 수: %d \n", LCount(&list));
	if (LFirst(&list, &data))
	{
		printf("%d ", data);

		while (LNext(&list, &data))
		{
			printf("%d ", data);
		}
	}
	printf("\n\n");

	return 0;
}

int CompareForSort(LData d1, LData d2)
{
	if (d1 > d2)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}