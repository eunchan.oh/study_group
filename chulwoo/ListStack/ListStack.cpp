#include <iostream>
#include "ListStack.h"

void StackInit(Stack * pstack)
{
	pstack->head = NULL;
}

int SIsEmpty(Stack * pstack)
{
	if (pstack->head == NULL)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

void SPush(Stack * pstack, Data data)
{
	Node * newNode = (Node*)malloc(sizeof(Node));

	newNode->data = data;
	newNode->next = pstack->head;
	pstack->head = newNode;
}

Data SPop(Stack * pstack)
{
	if (SIsEmpty(pstack))
	{
		std::cout << "Stack Memory Error." << std::endl;
		exit(-1);
	}
	else
	{
		Node * rmvNode = pstack->head;
		Data rmvData = pstack->head->data;

		pstack->head = pstack->head->next;

		free(rmvNode);

		return rmvData;
	}
}

Data SPeek(Stack * pstack)
{
	if (SIsEmpty(pstack))
	{
		std::cout << "Stack Memory Error." << std::endl;
		exit(-1);
	}

	return pstack->head->data;
}