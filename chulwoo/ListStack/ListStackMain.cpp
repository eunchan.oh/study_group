#include <iostream>
#include "ListStack.h"

using namespace std;

int main(void)
{
	Stack stack;

	StackInit(&stack);

	cout << "==Stack Push==" << endl;
	for (int i = 1; i <= 5; i++)
	{
		SPush(&stack, i);
		cout << i << endl;
	}

	cout << "==Stack Pop==" << endl;
	while (!SIsEmpty(&stack))
	{
		cout << SPop(&stack) << endl;
	}

	return 0;
}