#include <stdio.h>
#include <stdlib.h>
#include "CLinkedList.h"

void LInit(List * plist)
{
	plist->tail = NULL;
	plist->NumOfData = 0;
}

void LInsert(List * plist, LData data)
{
	Node * newNode = (Node*)malloc(sizeof(Node));
	newNode->data = data;

	if (plist->tail == NULL)
	{
		plist->tail = newNode;
		newNode->next = newNode;
	}
	else
	{
		newNode->next = plist->tail->next;
		plist->tail->next = newNode;
		plist->tail = newNode;
	}

	(plist->NumOfData)++;
}

void LInsertFront(List * plist, LData data)
{
	Node * newNode = (Node*)malloc(sizeof(Node));
	newNode->data = data;

	if (plist->tail == NULL)
	{
		plist->tail = newNode;
		newNode->next = newNode;
	}
	else
	{
		newNode->next = plist->tail->next;
		plist->tail->next = newNode;
	}

	(plist->NumOfData)++;
}

int LFirst(List * plist, LData * pdata)
{
	if (plist->tail == NULL)
	{
		return FALSE;
	}
	else
	{
		plist->before = plist->tail;
		plist->cur = plist->tail->next;

		*pdata = plist->cur->data;

		return TRUE;
	}
}

int LNext(List * plist, LData * pdata)
{
	if (plist->tail == plist->tail->next)
	{
		return FALSE;
	}
	else
	{
		plist->before = plist->cur;
		plist->cur = plist->cur->next;

		*pdata = plist->cur->data;

		return TRUE;
	}
}

LData LRemove(List * plist)
{
	Node * rmvNode = plist->cur;
	LData rmvData = plist->cur->data;

	if (rmvNode == plist->tail)
	{
		if (plist->tail == plist->tail->next)
		{
			plist->tail = NULL;
		}
		else
		{
			plist->tail = plist->before;
		}
	}
	plist->before->next = plist->cur->next;
	plist->cur = plist->before;
	
	free(rmvNode);
	(plist->NumOfData)--;

	return rmvData;
}

int LCount(List * plist)
{
	return plist->NumOfData;
}