#include <stdio.h>
#include "CLinkedList.h"

int main(void)
{
	List list;
	int data, i, NumOfNode;
	LData input;

	LInit(&list);

	// 삽입
	for (input = 1; input <= 10; input++)
	{
		LInsert(&list, input);
	}

	// 조회
	printf("리스트 데이터: ");
	if (LFirst(&list, &data))
	{
		printf("%d ", data);
		
		for (i = 0; i < LCount(&list) - 1; i++)
		{
			if (LNext(&list, &data))
			{
				printf("%d ", data);
			}
		}
	}
	printf("\n");
	
	// 삭제
	NumOfNode = LCount(&list);

	printf("삭제된 데이터: ");
	if (LFirst(&list, &data))
	{
		if (data % 2 == 0)
		{
			printf("%d ", LRemove(&list));
		}
		
		for (i = 0; i < NumOfNode - 1; i++)
		{
			if (LNext(&list, &data))
			{
				if (data % 2 == 0)
				{
					printf("%d ", LRemove(&list));
				}
			}
		}
	}
	printf("\n");

	// 삭제 후 조회
	printf("리스트 데이터: ");
	if (LFirst(&list, &data))
	{
		printf("%d ", data);

		for (i = 0; i < LCount(&list) - 1; i++)
		{
			if (LNext(&list, &data))
			{
				printf("%d ", data);
			}
		}
	}
	printf("\n");

	return 0;
}