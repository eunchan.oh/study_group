#ifndef _C_LINKEDLIST_H_
#define _C_LINKEDLIST_H_

#define TRUE 1
#define FALSE 0

typedef int LData;

typedef struct _node {
	LData data;
	struct _node * next;
}Node;

typedef struct _CircularLikedList {
	Node * tail;
	Node * cur;
	Node * before;
	int NumOfData;
}CLList;

typedef CLList List;

void LInit(List * plist);

void LInsert(List * plist, LData data);
void LInsertFront(List * plist, LData data);

int LFirst(List * plist, LData * data);
int LNext(List * plist, LData * data);

LData LRemove(List * plist);

int LCount(List * plist);

#endif