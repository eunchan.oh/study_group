// Stack 
// by eunchan.oh 

#include <iostream>
#include <fstream>
#include <stdio.h>

using namespace std;




class Stack
{
	private:
		int top;
		int size;
		int bottom;
		char* buf;
		
		
	public:
		Stack();
		Stack(int stack_size);
		~Stack();
		void Pop();
		void Push(int _data);
		void Show();
		bool isEmpty();
		bool isFull();
		
	
};
Stack::Stack()
{
	cout<<"Initiallize Default Constructor"<<endl;
	size = 5;
	buf = new char[size];
	top=-1;
	bottom=0;
	cout<<"Stack Size "<<size<<endl;
}

Stack::Stack(int stack_size)
{
	cout<<"###Initiallize Constructor"<<endl;
	size=stack_size;
	
	buf = new char[size];
	
	//size = 2;
	top=-1;			
	bottom=0;
	cout<<"Stack Size "<<size<<endl;
	
}

Stack::~Stack()
{
	cout<<"###Initiallize Destructor"<<endl;
	delete[] buf;
	
}
bool Stack:: isEmpty()
{
	//empty -> return true
	 
	if(top<bottom)
	{
		cout<<"Stack is Empty"<<endl;
		return true;
		
	}
	else
		return false;
	
}

bool Stack::isFull()
{
	// Full -> return true
	if(top+1>=size)
	{
		cout<<"Stack is Full"<<endl;
		return true;
		
	}
	else
		return false;
		
	
}

void Stack:: Pop()
{
	if(isEmpty())
		return;
		
	
	printf("\nDelete Data: %d \n",buf[top]);
	buf[top--]=0;
	
}

void Stack:: Push(int _data)
{
	if(isFull())
		return;
		
	
	cout<<"data "<<_data<<endl;	
	
	if(top<= size)
		buf[++top]=_data;
	

	printf("\ntop: %d\n",top);
	
	
	
	
}

void Stack::Show()
{
	//printf("\nbuf %d\n",buf[0]);
	
	if(isEmpty())
		return;
	
	printf("\n[Stack Size]: %d\n",size);
	printf("\n\n======================\n");
	for(int i=0; i<=top; i++)
		printf("   index[%d]:   %d\n",i,buf[i]);
	
	printf("======================\n");
		
}


int main()
{
	
	int stack_size;
	int select;
	
	//ifstream menu_print;
	//menu_print.open("menu.txt");	
	//cout<<menu_print<<endl;
	
	cout<<"Insert Stack Size:  ";
	cin>>stack_size;
	Stack stack=Stack(stack_size);
	
	//Stack stack;
	
	
	
	while(1)
	{
		cout<<"select menu number:  ";
		cin>>select;
		
		
		switch(select)
		{
			case 1:
				int data;
				cout<<"insert data: ";
				cin>>data;
				stack.Push(data);
				break;
				
			case 2:
				stack.Pop();
				break;
				
			case 3:
				stack.Show();
				break;
				
			case 4:
				return 0;
				
			default:
				cout<<"[1] Insert Data [2] Delete Data [3] Show Data [4] Exit"<<endl;
				
			
			
		}		
	
	}
	

	
	return 0;
	
	
}
