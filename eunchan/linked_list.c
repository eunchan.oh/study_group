#include <stdio.h>
#include <stdlib.h>
#include <string.h>


typedef struct node
{
	struct node* next;
	int data;

}Node;

void Add_Node(Node **t_head,int value)
{
	Node * new_node = (Node*)malloc(sizeof(Node));
	
	new_node->data = value;
	new_node->next = NULL;
	//printf("newnode %d\n\n", new_node);

	if (*t_head == NULL)
	{
		printf("First Node \n");
		*t_head = new_node;
		
	}
	else
	{
		
		Node *cur = *t_head;
		printf("Node is not empty\n");
		while (cur->next != NULL)	//cur 변수를 통해서 마지막 노드를 찾는 과정
		{
			cur = cur->next;
			
		}
		cur->next = new_node;	//기존 노드와 새노드를 연결
	}
}     

void Print_node(Node **t_head)
{
	Node * cur = NULL;

	cur = *t_head;
	
	while (cur != NULL)
	{
		printf("Data: %d\n", cur->data);
		cur = cur->next;
	}
	
}

void Delete_Node(Node** t_head, int t_data)
{

	Node * cur = *t_head;
	
	//Case 1
	//Node is empty
	if (*t_head == NULL)
	{
		printf("NODE is Empty\n");
		return;
	}
	
	//Case 2
	// Find is One node
	if (cur->data == t_data)
	{
		printf("노드가 1개일때\n");
		//cur = cur->next;
		*t_head = cur->next;
		free(cur);
		return;
	}
	
	//Else Case 

	//printf("test %d\n",cur->next->data);
	
	while (cur->next != NULL)
	{
		if (cur->next->data == t_data)
		{
			Node * del = cur->next;	
			
			if(del->next != NULL)
			{
				cur->next = del->next;
			
			}
			else
			{
				cur->next = NULL;		
			}
			free(del);
			return;	
		}
		cur = cur->next;

	}
	//free(cur);

}

int main()
{
	Node * head = NULL;

	int select = 0;
	int insert_data = 0;
	int delete_data = 0;
	
	
	while (1)
	{
		
		printf("[1].ADD Node  [2].Delete Node  [3].Output Node [4].Exit\n");
		scanf("%d", &select);
		
		switch (select)
		{
			case 1:
				printf("Insert Data: ");
				scanf("%d", &insert_data);
				Add_Node(&head,insert_data);
				break;

			case 2:
				printf("Delete Data: ");
				scanf("%d", &delete_data);
				Delete_Node(&head, delete_data);
				break;

			case 3:
				Print_node(&head);
				break;

			case 4:
				return;
				

			default:
				printf("Please re-enter.\n");
				break;
		}
	}
	
	return 0;
}
